# encoding: UTF-8
# frozen_string_literal: true
# ============================================================================
# Summary: run some tests against arduino-pulser to check for
#          correct pulse rate
# ============================================================================
require 'serialport'

@last_num_pulses = 0
@last_num_ticks  = 0
@start_time      = Time.now
@tick_width      = 1

# --------------------
def read_response
  line = ''.dup
  loop do
    ch = @mc.getbyte
    if ch.nil?
        # uncomment to debug
        # puts "DBG ch:nil"
        # puts "DBG signals:#{@mc.signals}"
        return
    end
    # puts "DBG ch:#{ch.ord} signals:#{@mc.signals}"

    next if ch.ord.to_i == 0x0D

    # the response ends on a linefeed from the Arduino
    break if ch.ord.to_i == 0x0A

    line << ch.chr
  end
  puts "     rx   : #{line}"
  line
end

# --------------------
def read_go_response
  line = read_response
  line =~ /ACK (\d+) (\d+)/
  @pulses_per_slot = Regexp.last_match(1).to_i
  @num_slots       = Regexp.last_match(2).to_i
end

# --------------------
def read_stats_response
  stats = read_response

  stats =~ /ACK (\d+) (\d+) (\d+) (\d+) (\d+)/
  num_pulses  = Regexp.last_match(1).to_f
  last_pulses = Regexp.last_match(2).to_f
  num_ticks   = Regexp.last_match(3).to_f
  last_ticks  = Regexp.last_match(4).to_f
  @tick_width = Regexp.last_match(5).to_f
  puts format('     stats: curr pulses=%10d ticks=%10d width=%10d', num_pulses, num_ticks, @tick_width)
  if num_ticks.positive?
    total_time = (num_ticks / 1000.0) * @tick_width
    pps        = num_pulses / total_time
    puts format('     stats: curr pps  : %10.3f time: %10.3fs', pps, total_time)
  else
    puts '     stats: curr pps  : n/a, num ticks is 0'
  end

  puts format('     stats: last pulses=%10d ticks=%10d width=%10d', last_pulses, last_ticks, @tick_width)
  if last_ticks.positive?
    total_time = (last_ticks / 1000.0) * @tick_width
    pps        = last_pulses / total_time
    puts format('     stats: last pps  : %10.3f time: %10.3fs', pps, total_time)
  else
    puts '     stats: last pps  : n/a, num ticks is 0'
  end

  err = 0
  if num_pulses < @last_num_pulses
    puts 'ERR  num_pulses has wrapped?'
    puts "     @last_num_pulses = #{@last_num_pulses}"
    err += 1
  end

  if num_ticks < @last_num_ticks
    puts 'ERR  num_ticks has wrapped?'
    puts "     @last_num_ticks = #{@last_num_ticks}"
    err += 1
  end

  if err.positive?
    puts 'exiting'
    exit(1)
  end
  @last_num_pulses = num_pulses
  @last_num_ticks  = num_ticks
end

# --------------------
def setup
  puts '---- setting up connection'

  # open the port with 115200 81N
  @port  = '/dev/ttyUSB0'
  parity = SerialPort::NONE
  @mc    = SerialPort.new(@port, 115_200, 8, 1, parity)

  puts '     flushing connection'
  # temporarily set the read timeout to non-blocking
  @mc.read_timeout = -1
  @mc.flush_input
  @mc.flush_output

  # reset the read_time to 1s
  @mc.read_timeout = 1000

  # read a few times to get rid of garbage
  @mc.putc 0x0D
  @mc.putc 0x0A
  loop do
    ch = @mc.getbyte
    break if ch.nil? || ch.ord.to_i == 0x0A
  end
  puts
end

# --------------------
def get_stats
  @mc.putc 'n'
  @mc.putc 0x0A
  puts "---- get stats at #{(Time.now - @start_time).to_i} seconds"
  read_stats_response
  puts
end


# --------------------
def do_stop
  puts '---- stop'
  # since stop is the first command sent, use it as a way to ensure
  # communication is okay
  loop do
    @mc.putc 's'
    @mc.putc 0x0A
    line = read_response
    break if line == 's ACK'
    puts "     retrying..."
    # after an upload, it normally takes about 4 retries...
  end

  @last_num_pulses = 0
  @last_num_ticks  = 0
  @start_time      = Time.now
  puts
end

# --------------------
def do_go(num_pulses)
  @last_num_pulses = 0
  @last_num_ticks  = 0

  puts "---- go pulses: #{num_pulses}"
  @mc.putc 'g'
  @mc.print num_pulses.to_s
  @mc.putc 0x0A
  @start_time      = Time.now
  read_go_response

  # 110 pulses / 10 slots = 11 pulses per slot
  # 1 slot every 100ms
  # 10 slots per second
  # 110 pulses per second

  expected_pulses_per_slot = num_pulses.to_f / @num_slots
  puts "     expected pulses/slot: #{expected_pulses_per_slot}"
  puts "     actual pulses/slot  : #{@pulses_per_slot}"
  puts "     1 slot every #{@tick_width}ms and #{@num_slots} slots"
  pps = (1000.0 * @pulses_per_slot) / @tick_width
  puts "     pulses per second (pps): #{pps}"
  puts
end

# >> MAIN --------------------
$stdout.sync = true
$stdin.sync  = true

@pulses_per_slot = 0
@num_slots       = 1

setup

# just in case the script was restarted
do_stop

# send get stats and check all is 0
get_stats

sleep(0.1)

# do not set too low:
#   - it has to be higher than the number of slots
#   - e.g. g9 reports 0pps amd g10 reports 10pps
#   - g10 is the min
# do not set too high:
#   - too many pulses can cause the slot to be higher than a max integer value
#   - too many pulses cause the ISR to take too long to execute
#   - e.g. g32761 reports 32760pps and  g32770 reports 0pps
#   - g32760 pps is the max

# 110 works fine
do_go(110)

sleep(0.1)
3.times do
  get_stats
  sleep(1)
end

do_stop

# ask for stats one last time
sleep(0.1)
get_stats

@mc.close
