// --------------------
// Pulse a pin at a given rate
// for a specific total number of pulses
// --------------------
#include <avr/interrupt.h>
#include <wiring_private.h>

// --------------------
struct pulser_info_t {
    // the pin to pulse
    int pin_num = 0;

    // assume a base timer of 2mS
    // using a prescaler of 50, will cause the ISR content
    // to be executed only every 100ms.
    // NOTE: do not set this to 0 or 1
#define PULSER_PRESCALER 50
    int prescaler = 0;

    int pulse_index = 0;
#define PULSER_ARRAY_WIDTH 10
    int pulses[PULSER_ARRAY_WIDTH] = {};
    int per_slot = 0;

    // current and last number of pulses that have been done
    unsigned long curr_num_pulses = 0;
    unsigned long last_num_pulses = 0;

    // current and last number of interrupt ticks
    unsigned long curr_num_ticks = 0;
    unsigned long last_num_ticks = 0;

    // how long between interrupts (ms)
    unsigned int tick_time = 0;
};
static pulser_info_t pulser_info;

void pulser_clear();

// --------------------
void pulser_init()
{
    // the pin to pulse
    pulser_info.pin_num = 10;

    // set up pin as an output
    pinMode(pulser_info.pin_num, OUTPUT);

    pulser_clear();

    pulser_info.curr_num_pulses = 0;
    pulser_info.last_num_pulses = 0;

    pulser_info.curr_num_ticks = 0;
    pulser_info.last_num_ticks = 0;

    // tick time is timer period * prescaler value
    pulser_info.tick_time = 2 * PULSER_PRESCALER;

    // disable interrupts
    cli();

    // Note: you must set TCCR2A before
    // setting TCCR2B!
    // set WGM to CTC mode (010)
    TCCR2A = _BV(WGM21);

    // set prescaler to 128
    TCCR2B = 0;
    TCCR2B = _BV(CS22) | _BV(CS20);

    // set to 2ms
    //    1/16Mhz = 62.5nS
    //    62.5 * 128 = 8000
    //    8000 * (249 + 1) = 2000000nS = 2mS
    OCR2A = 249;

    // when Timer2 count matches OCR2A, interrupt occurs
    TIMSK2 = _BV(OCIE2A);

    // enable interrupts
    sei();
}

// --------------------
// clear the pulse array
void pulser_clear()
{
    pulser_info.prescaler = 0;
    pulser_info.pulse_index = 0;
    for (int i = 0; i < PULSER_ARRAY_WIDTH; ++i) {
        pulser_info.pulses[i] = 0;
    }
    pulser_info.per_slot = 0;
}

// --------------------
void pulser_save()
{
    // save the previous num pulses
    pulser_info.last_num_pulses = pulser_info.curr_num_pulses;
    pulser_info.curr_num_pulses = 0;

    // save the previous num ticks
    pulser_info.last_num_ticks = pulser_info.curr_num_ticks;
    pulser_info.curr_num_ticks = 0;
}

// --------------------
void pulser_set_pulses_to(int gocount)
{
    // set pulse array to count, reset variables, and go
    pulser_info.per_slot = gocount / PULSER_ARRAY_WIDTH;
    for (int i = 0; i < PULSER_ARRAY_WIDTH; ++i) {
        pulser_info.pulses[i] = pulser_info.per_slot;
    }

    // NOTE: there may be some remainder that is not accounted for
}

// --------------------
void pulser_go(int gocount)
{
    cli();

    // ensure the pin is off
    cbi(PORTB, PORTB2);

    pulser_clear();
    pulser_set_pulses_to(gocount);
    pulser_save();

    sei();
}

// --------------------
void pulser_stop()
{
    cli();

    // ensure the pin is off
    cbi(PORTB, PORTB2);

    // save current data and clear the array for the next run
    pulser_clear();
    pulser_save();

    sei();
}

// --------------------
// interrupt service routine (ISR) is called when the timer interrupt
ISR(TIMER2_COMPA_vect)
{
    ++pulser_info.prescaler;
    if (pulser_info.prescaler < PULSER_PRESCALER) {
        return;
    }
    pulser_info.prescaler = 0;

    ++pulser_info.curr_num_ticks;

    // each pulse slot may indicate multiple pulses
    for (int pulse_count = 0; pulse_count < pulser_info.pulses[pulser_info.pulse_index]; ++pulse_count) {
        // assume the pin is off, pulse for n uS
        sbi(PORTB, PORTB2);
        // TODO: get stepper minimum pulse width
        delayMicroseconds(10);
        cbi(PORTB, PORTB2);
        // TODO: check if the stepper needs a minimum reset time
        // before the start of the next pulse

        // TODO: check if the total number of pulses is
        // longer than the ISR period

        // track the total number of pulses
        ++pulser_info.curr_num_pulses;
    }

    // bump to the next pulse slot
    // it's circular, so wrap to 0 if needed
    ++pulser_info.pulse_index;
    if (pulser_info.pulse_index >= PULSER_ARRAY_WIDTH) {
        pulser_info.pulse_index = 0;
    }
}

// --------------------
enum parser_states_t {
    PARSER_START,
    PARSER_GO_PCOUNT,
    PARSER_STATS,
    PARSER_STOP,
};

// --------------------
struct parser_info_t {
    enum parser_states_t state = PARSER_START;
    int pcount = 0;
};
static parser_info_t parser_info;

// --------------------
void parser_init()
{
    parser_info.state = PARSER_START;
    parser_info.pcount = 0;
}

// --------------------
void print_long(unsigned long i)
{
    char buf[25];
    sprintf(buf, "%lu", i);
    Serial.print(buf);
}

// --------------------
void parser_handle_eol()
{
    switch (parser_info.state) {
    case PARSER_START: {
        // ignore extra carriage returns
        break;
    }

    case PARSER_GO_PCOUNT: {
        pulser_go(parser_info.pcount);
        // the round off causes the per_slot value to be
        // 0 even if you use "g1" or "g2". It only
        // becomes 1 or more when you use a multiple of
        // PULSER_ARRAY_WIDTH. So on the ACK, give an
        // indication that it's changed.
        Serial.print("g ACK ");
        Serial.print(pulser_info.per_slot, DEC);
        Serial.print(" ");
        Serial.println(PULSER_ARRAY_WIDTH, DEC);
        break;
    }

    case PARSER_STOP: {
        pulser_stop();
        Serial.println("s ACK");
        break;
    }

    case PARSER_STATS: {
        // save a copy of all the relevant variables
        // Note: during this operation, there may be an ISR
        // call. So the values are not guaranteed to be consistent.

        // current and last number of pulses that have been done
        unsigned long tmp_curr_num_pulses = pulser_info.curr_num_pulses;
        unsigned long tmp_last_num_pulses = pulser_info.last_num_pulses;

        // current and last number of interrupt ticks
        unsigned long tmp_curr_num_ticks = pulser_info.curr_num_ticks;
        unsigned long tmp_last_num_ticks = pulser_info.last_num_ticks;

        // how long between interrupts (ms)
        unsigned int tmp_tick_time = pulser_info.tick_time;

        // send the stats
        Serial.print("n ACK ");
        print_long(tmp_curr_num_pulses);
        Serial.print(" ");
        print_long(tmp_last_num_pulses);
        Serial.print(" ");
        print_long(tmp_curr_num_ticks);
        Serial.print(" ");
        print_long(tmp_last_num_ticks);
        Serial.print(" ");
        Serial.print(tmp_tick_time, DEC);
        Serial.println();
        break;
    }

    default: {
        Serial.println("NAK bad parser state");
        break;
    }
    }

    // in all cases, reset parser state
    parser_init();
}

// --------------------
void parser_handle_ch(char ch)
{
    switch (ch) {
    // s - stop
    case 's': {
        if (parser_info.state == PARSER_START) {
            parser_info.state = PARSER_STOP;
        } else {
            Serial.println("NAK unexpected character 's'");
            parser_init();
        }
        break;
    }

    // n - stats
    case 'n': {
        if (parser_info.state == PARSER_START) {
            parser_info.state = PARSER_STATS;
        } else {
            Serial.println("NAK unexpected character 'n'");
            parser_init();
        }
        break;
    }

    // g - go!
    case 'g': {
        if (parser_info.state == PARSER_START) {
            parser_info.state = PARSER_GO_PCOUNT;
        } else {
            Serial.println("NAK unexpected character 'g'");
            parser_init();
        }
        break;
    }

    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9': {
        if (parser_info.state == PARSER_GO_PCOUNT) {
            parser_info.state = PARSER_GO_PCOUNT;
            parser_info.pcount = (parser_info.pcount * 10) + ch - '0';
        } else {
            Serial.println("NAK unexpected character (0-9)");
            parser_init();
        }
        break;
    }

    case 0x0A: {
        parser_handle_eol();
        break;
    }

    case 0x0D: {
        // ignore
        break;
    }

    default:
        if (parser_info.state == PARSER_START) {
            Serial.println("NAK unknown command");
        } else {
            Serial.println("NAK bad param, unknown character");
        }
        parser_init();
    }
}

// --------------------
// cppcheck-suppress unusedFunction
void setup()
{
    // open the serial port at 115200 bps
    Serial.begin(115200);

    parser_init();
    pulser_init();
}

// --------------------
// cppcheck-suppress unusedFunction
void loop()
{
    // skip if nothing to read
    if (Serial.available() <= 0) {
        return;
    }

    // read the incoming byte:
    char ch = (char)Serial.read();

    // run the parser
    parser_handle_ch(ch);
}
