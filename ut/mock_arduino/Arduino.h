#ifndef _ARDUINO_H
#define _ARDUINO_H

#define DEC 10

class MockSerial {
public:
    MockSerial();
    void begin(int num);
    char read();
    void print(const char* s);
    void print(const char c);
    void print(unsigned int, int);
    void println();
    void println(const char* s);
    void println(int, int);
    int available();
};

extern MockSerial Serial;

#define OUTPUT 0
void pinMode(int pin, int mode);

void delayMicroseconds(int delay);

#endif
