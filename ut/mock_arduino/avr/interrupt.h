#ifndef _INTERRUPT_H
#define _INTERRUPT_H
#define ISR(name) void isr_##name(void)
#define PORTB 0
#define PORTB2 2

extern void sei(void);
extern void cli(void);
#endif
