#include "mock_arduino.h"
#include "Arduino.h"
#include "avr/interrupt.h"
#include <stdio.h>
#include <string.h>

int TCCR2B = 0;
int OCR2A = 0;
int TCCR2A = 0;
int TIMSK2 = 0;

mock_values_t mock_values;

void mock_init()
{
    mock_values.cbi_called_count = 0;
    mock_values.sbi_called_count = 0;
    mock_values.cli_called_count = 0;
    mock_values.sei_called_count = 0;
    mock_clear_serial_out();
}

void mock_clear_serial_out()
{
    memset(mock_values.serial_out, 0x00, sizeof(mock_values.serial_out));
}

MockSerial Serial;

MockSerial::MockSerial()
{
}

void MockSerial::begin(int num)
{
    (void)num;
}

char MockSerial::read()
{
    return 0x00;
}

void MockSerial::print(const char* s)
{
    strcat(mock_values.serial_out, s);
}

void MockSerial::print(const char c)
{
    (void)c;
    // TODO:  strcat(mock_values.serial_out, );
}

void MockSerial::print(unsigned int v, int format)
{
    char buf[30];
    if (format == DEC) {
        sprintf(buf, "%d", v);
    } else {
        sprintf(buf, "%X", v);
    }
    strcat(mock_values.serial_out, buf);
}

void MockSerial::println()
{
    strcat(mock_values.serial_out, "\x0A");
}

void MockSerial::println(const char* s)
{
    print(s);
    strcat(mock_values.serial_out, "\x0A");
}

void MockSerial::println(int v, int format)
{
    print(v, format);
    strcat(mock_values.serial_out, "\x0A");
}

int MockSerial::available()
{
    return 1;
}

void pinMode(int pin, int mode)
{
    (void)pin;
    (void)mode;
}

void cbi(int port, int bit)
{
    (void)port;
    (void)bit;
    mock_values.cbi_called_count++;
}

void sbi(int port, int bit)
{
    (void)port;
    (void)bit;
    mock_values.sbi_called_count++;
}

void sei(void)
{
    mock_values.sei_called_count++;
}

void cli(void)
{
    mock_values.cli_called_count++;
}

void delayMicroseconds(int delay)
{
    (void)delay;
}
