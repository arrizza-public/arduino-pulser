#ifndef _MOCK_H
#define _MOCK_H

#define __COMPILING_AVR_LIBC__
#define F_CPU 16000000L
#define ARDUINO 10005
#define ARDUINO_
#define ARDUINO_ARCH_AVR

#ifdef __AVR_ATmega2560__
    #undef __AVR_ATmega2560__
    #define __AVR_ATmega2560__
#endif

#ifdef __THROW
    #undef __THROW
    #define __THROW
#endif

#include "Arduino.h"

struct _mock_values {
    int cbi_called_count;
    int sbi_called_count;
    int sei_called_count;
    int cli_called_count;
    char serial_out[2048];
};
typedef struct _mock_values mock_values_t;
extern mock_values_t mock_values;

void mock_init();
void mock_clear_serial_out();

#define _BV(v) v

#define CS20 0b001
#define CS21 0b010
#define CS22 0b100
extern int TCCR2B;

extern int OCR2A;

#define WGM21 0b001
extern int TCCR2A;

#define OCIE2A 0b001;
extern int TIMSK2;

#endif
