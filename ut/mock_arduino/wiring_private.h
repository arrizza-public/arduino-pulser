#ifndef _wiring_h
#define _wiring_h

extern void sbi(int port, int bit);
extern void cbi(int port, int bit);

#endif
