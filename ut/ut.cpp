#include "gtest/gtest.h"

#include "Arduino.h"
#include "mock_arduino/mock_arduino.h"
// must be last
#include "../src/main.cpp"

// --------------------
// simulate running the ISR one tick
void run_isr_one_tick()
{
    for (int i = 0; i < PULSER_PRESCALER; ++i) {
        isr_TIMER2_COMPA_vect();
    }
}

// --------------------
TEST(pulser_info, init)
{
    mock_init();
    setup();

    EXPECT_EQ(10, pulser_info.pin_num);
    EXPECT_EQ(0, pulser_info.prescaler);

    EXPECT_EQ(0, pulser_info.curr_num_pulses);
    EXPECT_EQ(0, pulser_info.last_num_pulses);
    EXPECT_EQ(0, pulser_info.curr_num_ticks);
    EXPECT_EQ(0, pulser_info.last_num_ticks);
    EXPECT_EQ(100, pulser_info.tick_time);

    EXPECT_EQ(PARSER_START, parser_info.state);
}

// --------------------
TEST(stop, happy_path)
{
    mock_init();
    setup();

    // sim running for a while
    pulser_info.curr_num_pulses = 100;
    pulser_info.curr_num_ticks = 150;

    EXPECT_EQ(PARSER_START, parser_info.state);

    parser_handle_ch('s');
    EXPECT_EQ(PARSER_STOP, parser_info.state);
    // extra cr is ignored
    parser_handle_ch(0x0D);
    parser_handle_ch(0x0A);

    EXPECT_STREQ("s ACK\x0A", mock_values.serial_out);

    // confirm stop was done
    EXPECT_EQ(100, pulser_info.last_num_pulses);
    EXPECT_EQ(150, pulser_info.last_num_ticks);

    EXPECT_EQ(0, pulser_info.curr_num_pulses);
    EXPECT_EQ(0, pulser_info.curr_num_ticks);
    EXPECT_EQ(PARSER_START, parser_info.state);
    for (
        int i = 0;
        i < PULSER_ARRAY_WIDTH; ++i) {
        EXPECT_EQ(0, pulser_info.pulses[i]);
    }

    // the set/clear interrupts were called twice
    // once in the pulser init and once by stop
    EXPECT_EQ(2, mock_values.sei_called_count);
    EXPECT_EQ(2, mock_values.cli_called_count);
}

// --------------------
TEST(stop, bad_format)
{
    mock_init();
    setup();
    parser_handle_ch('s');
    parser_handle_ch('1');
    parser_handle_ch(0x0A);

    EXPECT_STREQ("NAK unexpected character (0-9)\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);

    mock_clear_serial_out();
    parser_handle_ch('s');
    parser_handle_ch('g');
    parser_handle_ch(0x0A);

    EXPECT_STREQ("NAK unexpected character 'g'\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);
}

// --------------------
TEST(isr, happy_path)
{
    mock_init();
    setup();

    EXPECT_EQ(0, pulser_info.curr_num_pulses);
    EXPECT_EQ(0, pulser_info.curr_num_ticks);
    EXPECT_EQ(0, pulser_info.pulse_index);

    EXPECT_EQ(0, pulser_info.pulse_index);
    run_isr_one_tick();

    // each ISR call bumps the tick count, but
    // the pulses count may be 0
    EXPECT_EQ(0, pulser_info.curr_num_pulses);
    EXPECT_EQ(1, pulser_info.curr_num_ticks);
    EXPECT_EQ(1, pulser_info.pulse_index);

    for (
        int i = 0;
        i < PULSER_ARRAY_WIDTH; ++i) {
        pulser_info.pulses[i] = i + 1;
    }

    run_isr_one_tick();
    EXPECT_EQ(2, pulser_info.curr_num_ticks);
    EXPECT_EQ(2, pulser_info.pulse_index);

    // we should be setting and clearing the bit
    // the same number of times as the number of pulses
    EXPECT_EQ(2, pulser_info.curr_num_pulses);
    EXPECT_EQ(2, mock_values.cbi_called_count);
    EXPECT_EQ(2, mock_values.sbi_called_count);

    pulser_info.pulses[2] = 123;
    run_isr_one_tick();
    EXPECT_EQ(3, pulser_info.curr_num_ticks);
    EXPECT_EQ(3, pulser_info.pulse_index);

    EXPECT_EQ(125, pulser_info.curr_num_pulses);
    EXPECT_EQ(125, mock_values.cbi_called_count);
    EXPECT_EQ(125, mock_values.sbi_called_count);
}

// --------------------
TEST(stats, happy_path)
{
    mock_init();
    setup();

    // do some pulsing
    for (
        int i = 0;
        i < PULSER_ARRAY_WIDTH; ++i) {
        pulser_info.pulses[i] = i + 1;
    }

    // run the isr to get the pulsing to occur
    run_isr_one_tick();

    parser_handle_ch('n');
    EXPECT_EQ(PARSER_STATS, parser_info.state);
    parser_handle_ch(0x0A);

    EXPECT_STREQ("n ACK 1 0 1 0 100\x0A", mock_values.serial_out);

    mock_clear_serial_out();

    // run the isr to get the pulsing to occur
    run_isr_one_tick();

    parser_handle_ch('n');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("n ACK 3 0 2 0 100\x0A", mock_values.serial_out);

    mock_clear_serial_out();
    parser_handle_ch('s');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("s ACK\x0A", mock_values.serial_out);

    mock_clear_serial_out();
    parser_handle_ch('n');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("n ACK 0 3 0 2 100\x0A", mock_values.serial_out);
}

// --------------------
TEST(stats, bad_format)
{
    mock_init();
    setup();
    parser_handle_ch('n');
    parser_handle_ch('1');
    parser_handle_ch(0x0A);

    EXPECT_STREQ("NAK unexpected character (0-9)\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);

    mock_clear_serial_out();
    parser_handle_ch('n');
    parser_handle_ch('s');
    parser_handle_ch(0x0A);

    EXPECT_STREQ("NAK unexpected character 's'\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);
}

// --------------------
TEST(go, happy_path)
{
    mock_init();
    setup();

    parser_handle_ch('g');
    parser_handle_ch('4');
    parser_handle_ch('2');
    parser_handle_ch(0x0A);

    EXPECT_STREQ("g ACK 4 10\x0A", mock_values.serial_out);

    int pulse_count = 0;
    for (
        int i = 0;
        i < PULSER_ARRAY_WIDTH; ++i) {
        pulse_count += pulser_info.pulses[i];
    }

    // for now, ignore remainder
    int exp_count = (42 / PULSER_ARRAY_WIDTH) * PULSER_ARRAY_WIDTH;

    EXPECT_EQ(exp_count, pulse_count);
}

// --------------------
TEST(go, bad_format)
{
    mock_init();
    setup();
    parser_handle_ch('g');
    parser_handle_ch('1');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("g ACK 0 10\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);

    mock_clear_serial_out();
    parser_handle_ch('g');
    parser_handle_ch(' ');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("NAK bad param, unknown character\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);

    mock_clear_serial_out();
    parser_handle_ch('g');
    parser_handle_ch('1');
    parser_handle_ch('s');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("NAK unexpected character 's'\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);

    mock_clear_serial_out();
    parser_handle_ch('g');
    parser_handle_ch('n');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("NAK unexpected character 'n'\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);
}

// --------------------
TEST(parser, bad_command)
{
    mock_init();
    mock_init();
    setup();
    parser_handle_ch('z');
    parser_handle_ch(0x0A);
    EXPECT_STREQ("NAK unknown command\x0A", mock_values.serial_out);
    EXPECT_EQ(PARSER_START, parser_info.state);
}

// --------------------
TEST(loop, happy_path)
{
    mock_init();
    loop();
    // incoming char is a 0x00 so still in Start state
    EXPECT_EQ(PARSER_START, parser_info.state);
}
